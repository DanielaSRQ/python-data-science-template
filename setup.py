import setuptools

setuptools.setup(
    name="src",
    version="0.0.1",
    packages=setuptools.find_packages(),
    python_requires=">=3.11",
)
